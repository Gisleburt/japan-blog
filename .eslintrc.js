module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb',
    'plugin:@next/next/recommended',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  ignorePatterns: [
    'pages/index.jsx',
    'pages/archive.jsx',
  ],
  rules: {
    'react/jsx-props-no-spreading': 'off', // For now, until we set up TypeScript
    'react/jsx-one-expression-per-line': 'off', // This is a dumb rule
    'react/function-component-definition': ['error', { namedComponents: 'arrow-function' }],
    'jsx-a11y/anchor-is-valid': 'off', // Next JS adds these via Links
    'max-len': ['error', { code: 120 }],
  },
};
