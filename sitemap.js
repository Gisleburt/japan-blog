const fs = require('fs');
const sm = require('sitemap');
const pages = require('./pages');
const config = require('./config');

const urlsBase = [{ url: '/' }];
const urls = pages
  .map((name) => `/${name}`) // Add a slash to the beginning
  .reduce((acc, url) => [...acc, { url }], urlsBase);

const sitemap = sm.createSitemap({
  urls,
  hostname: config.domain,
  cacheTime: 600000,
});

fs.writeFileSync('out/sitemap.xml', sitemap.toString());
