const fs = require('fs');
const pageNames = require('./pages');

const latest = pageNames
  .filter(page => page.match(/\d{4}-\d{2}-\d{2}/))
  .pop();

const output = `/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import Script from 'next/script';

const Index = () => (
  <Script>
      window.location.replace("/${latest}");
  </Script>
);

export default Index;
`;

fs.writeFileSync('pages/index.jsx', output);
