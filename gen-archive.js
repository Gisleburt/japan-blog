const fs = require('fs');
const pageNames = require('./pages');
const config = require('./config');
const fullTrim = require('./helpers/full-trim');

const pages = pageNames
  .filter((page) => page.match(/\d{4}-\d{2}-\d{2}/))
  .reverse()
  .map((pageName) => require(`./config/${pageName}.js`));

const pageToLi = ({
  path,
  title,
  description,
  image,
}) => (`
      <li>
        <style jsx>
          {\`
            li {
              margin-bottom: 20px;
            }

            li:nth-child(even) a {
              flex-direction: row-reverse;
            }

            .archive-image {
              background: url("${image}") center;
              background-size: cover;
              height: 200px;
              width: 300px;
            }

            .description {
              padding: 16px;
              font-family: sans-serif;
              flex: 1;
            }

            a {
              display: flex;
              box-shadow: 2px 2px 10px 5px rgba(0, 0, 0, .2);
            }

            header {
              font-size: 1.2em;
            }

            @media ${config.css.breakpoint.medium} {
              header {
                font-size: 1.6rem;
              }
            }
    
            @media ${config.css.breakpoint.large} {
              header {
                font-size: 2rem;
              }
            }
          \`}
        </style>
        <Link href="/${path}">
          <a className="container">
            <div className="archive-image" />
            <div className="description">
              <header>${path} - ${title.english}</header>
              <p>
                ${fullTrim(description)}
              </p>
            </div>
          </a>
        </Link>
      </li>
`);

const output = `
import Link from 'next/link';
import React from 'react';
import Page from '../components/page';

const title = {
  english: 'Archive',
};

const Archive = () => (
  <Page title={title} description="The backlog of blog posts" path="archive">
    <ol>${pages.map(pageToLi).join('')}
    </ol>
  </Page>
);

export default Archive;
`;

fs.writeFileSync('pages/archive.jsx', output);
