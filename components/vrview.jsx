/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import Script from 'next/script';

class VrViewComponent extends React.Component {
  static cleanOptions(obj) {
    return Object.keys(obj).reduce((acc, cur) => {
      if (cur === 'id') {
        return acc;
      }
      if (obj[cur]) {
        if (cur === 'video' || cur === 'image') {
          acc[cur] = `https://monogatari.life${obj[cur]}`;
        } else {
          acc[cur] = obj[cur];
        }
      }
      return acc;
    }, {});
  }

  componentDidMount() {
    const { VRView } = window;
    const { id } = this.props;
    const playerOptions = VrViewComponent.cleanOptions(this.props);
    if (VRView) {
      // eslint-disable-next-line react/no-unused-state
      this.setState({ vrView: new VRView.Player(`#${id}`, playerOptions) });
    }
  }

  render() {
    const { id } = this.props;
    return (
      <>
        <Script
          strategy=""
          key="vrview"
          src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"
        />
        <style jsx>
          {`
            #${id} iframe {
              width: 100%;
              height: 500px;
            }
          `}
        </style>
        <div id={id} className="VRViewViewport" />
      </>
    );
  }
}

VrViewComponent.propTypes = {
  id: PropTypes.string.isRequired,
  video: PropTypes.string,
  image: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  preview: PropTypes.string,
  is_stereo: PropTypes.bool,
  is_debug: PropTypes.bool,
  is_vr_off: PropTypes.bool,
  is_autopan_off: PropTypes.bool,
  default_yaw: PropTypes.number,
  is_yaw_only: PropTypes.bool,
};

VrViewComponent.defaultProps = {
  video: undefined,
  image: undefined,
  width: '100%',
  height: '500px',
  preview: undefined,
  is_stereo: false,
  is_debug: false,
  is_vr_off: false,
  is_autopan_off: false,
  default_yaw: undefined,
  is_yaw_only: false,
};

export default VrViewComponent;
