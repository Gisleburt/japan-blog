import React from 'react';
import PropTypes from 'prop-types';
import Header from './header';

const extractHeaderProps = ({
  title, description, image, path,
}) => ({
  title, description, image, path,
});

const Page = (props) => {
  const { children } = props;
  return (
    <>
      <style jsx>
        {`
          .outer {
            min-height: 100vh;
          }

          .content {
            max-width: 1000px;
            margin: auto;
            padding: 20px;
          }
          
          .description {
            font-size: 1.4rem;
          }
        `}
      </style>
      <style jsx global>
        {`
          .content h2 {
            font-size: 1.8rem;
            margin: 20px 0;
          }
        `}
      </style>
      <div className="outer">
        <Header {...extractHeaderProps(props)} />
        <div className="content">
          {children}
        </div>
      </div>
    </>
  );
};

Page.propTypes = {
  ...Header.propTypes,
  children: PropTypes.node.isRequired,
};

Page.defaultProps = Header.defaultProps;

export default Page;
