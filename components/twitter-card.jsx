import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

const TwitterCard = ({ title, description, image }) => (
  <Head>
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content={title} />
    <meta name="twitter:description" content={description} />
    <meta name="twitter:image" content={`https://monogatari.life${image}`} />
    <meta name="twitter:creator" content="@gisleburt" />
  </Head>
);

TwitterCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default TwitterCard;
