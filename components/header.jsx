import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

import Navigation from './navigation';
import TwitterCard from './twitter-card';
import OpenGraph from './open-graph';
import config from '../config';
import GlobalStyles from './global-styles';
import fullTrim from '../helpers/full-trim';
import GoogleTagManager from './google-tag-manager';

const titleGenerator = (title) => (
  `${title.english} | ${title.native}`
);

const Header = ({
  title, description, image, path,
}) => (
  <section>
    <Head>
      <title>{titleGenerator(title)}</title>
      <meta name="description" content={fullTrim(description)} />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
    </Head>
    <GoogleTagManager trackingId="UA-73943600-3" />
    <GlobalStyles />
    <TwitterCard title={title.english} description={fullTrim(description)} image={image} />
    <OpenGraph title={title.english} description={fullTrim(description)} image={image} path={path} />
    <style jsx>
      {`
        li {
          display: inline;
        }
        
        header {
          font-family: ${config.css.fontFamily.header.english};
          font-size: 2rem;
          color: ${config.css.color.title};

          display: flex;
          justify-content: center;
          align-items: center;

          flex-direction: column;
          height: 100vh;
          background: url("${image}") center;
          background-size: cover;
        }
        
        .native {
          font-family: ${config.css.fontFamily.header.native};
          font-size: 3rem;
        }
        
        header p {
          display: inline-block;
        }

        @media ${config.css.breakpoint.medium} {
          header {
            font-size: 4rem;
          }
          .native {
            font-size: 7rem;
          }
        }

        @media ${config.css.breakpoint.large} {
          header {
            font-size: 6rem;
          }
          .native {
            font-size: 9rem;
          }
        }
      `}
    </style>
    <header title={title.english}>
      {title && <p className="native">{title.native}</p>}
      <h1>{title.english}</h1>
    </header>
    <Navigation />
  </section>
);

Header.propTypes = {
  title: PropTypes.shape({
    english: PropTypes.string.isRequired,
    native: PropTypes.string,
  }),
  description: PropTypes.string,
  image: PropTypes.string,
  path: PropTypes.string.isRequired,
};

Header.defaultProps = {
  title: config.title,
  image: '/static/images/about/sakura-2686483_1920.jpg',
  description: 'A blog about a couple in Japan',
};

export default Header;
