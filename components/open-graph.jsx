import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

import config from '../config';

const OpenGraph = ({
  title, description, image, path,
}) => (
  <Head>
    <meta property="og:title" content={title} />
    <meta property="og:type" content="article" />
    <meta property="og:image" content={`${config.domain}${image}`} />
    <meta property="og:description" content={description} />
    <meta property="og:site_name" content="Monotagari Life" />
    <meta property="og:url" content={`${config.domain}/${path}`} />
  </Head>
);

OpenGraph.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default OpenGraph;
