import React from 'react';
import Link from 'next/link';

const Nav = () => (
  <>
    <style jsx>
      {`
        nav {
          font-family: 'Lobster', cursive;
          position: absolute;
          top: 0;
          font-size: 1rem;
        }
        nav a,
        nav a:visited {
          color: #333;
          text-decoration: none;
        }
        nav li {
          margin: 20px 30px;
          display: inline-block;
        }

        @media (min-width: 800px) {
          nav {
            font-size: 1.3rem;
          }
        }

        @media (min-width: 1200px) {
          nav {
            font-size: 2rem;
          }
        }
      `}
    </style>
    <nav>
      <ol>
        <li>
          <Link href="/">
            <a>Latest</a>
          </Link>
        </li>
        <li>
          <Link href="/archive">
            <a>Archive</a>
          </Link>
        </li>
        <li>
          <Link href="/about">
            <a>About</a>
          </Link>
        </li>
      </ol>
    </nav>
  </>
);

export default Nav;
