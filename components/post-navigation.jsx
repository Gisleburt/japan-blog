import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

const PostNavigation = ({ next, previous }) => (
  <nav>
    <p className="post-navigation">
      <style jsx>
        {`
          .post-navigation {
            display:flex;
            justify-content: space-between;
          }

          .post-navigation a,
          .post-navigation span {
            min-width: 105px;
            text-decoration: underline;
            text-align: center;
          }
          .previous::before {
            content: '<< ';
          }
          .next::after {
            content: ' >>';
          }
        `}
      </style>
      {previous ? <Link href={`/${previous}`}><a className="previous">Previous</a></Link> : <span />}
      <Link href="/archive"><a className="archive">Archive</a></Link>
      {next ? <Link href={`/${next}`}><a className="next">Next</a></Link> : <span />}
    </p>
  </nav>
);

PostNavigation.propTypes = {
  next: PropTypes.string,
  previous: PropTypes.string,
};

PostNavigation.defaultProps = {
  next: null,
  previous: null,
};

export default PostNavigation;
