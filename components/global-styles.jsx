import React from 'react';
import Head from 'next/head';

const cssReset = 'https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css';

const GlobalStyles = () => (
  <>
    <style jsx global>
      {`
        body p {
          font-family: sans-serif;
          margin: 20px 0;
          line-height: 1.5;
          font-size: 1.2em;
        }
        body a,
        body a:visited {
          color: #333;
          text-decoration: none;
        }
      `}
    </style>
    <Head>
      <link rel="stylesheet" type="text/css" href={cssReset} media="screen" />
      <link
        href="https://fonts.googleapis.com/css?family=Lobster|Noto+Serif+SC&display=optional"
        rel="stylesheet"
      />
    </Head>
  </>
);

export default GlobalStyles;
