import React from 'react';
import PropTypes from 'prop-types';
import Script from 'next/script';

const GoogleTagManager = ({ trackingId }) => (
  <>
    <Script strategy="lazyOnload" src={`https://www.googletagmanager.com/gtag/js?id=${trackingId}`} />
    <Script strategy="lazyOnload" id="initGoogleTagManager">
      {`
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '${trackingId}');
      `}
    </Script>
  </>
);

GoogleTagManager.propTypes = {
  trackingId: PropTypes.string.isRequired,
};

export default GoogleTagManager;
