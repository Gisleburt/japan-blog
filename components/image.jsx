import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Image = ({ alt, src }) => {
  const [show, setShow] = useState(false);
  const toggleShow = () => setShow(!show);
  return (
    <div
      className="image-container"
      onClick={toggleShow}
      onKeyPress={toggleShow}
      role="button"
      tabIndex="0"
    >
      <style jsx>
        {`
          .image-container {
            box-shadow: 2px 2px 10px 5px rgba(0, 0, 0, .2);
            margin: 20px 0;
          }

          .image-container p {
            margin: 2px 10px;
          }

          .image {
            width: 100%;
            background: url('${src}') center;
            background-size: cover;
            height: 550px;
            cursor: pointer;
          }

          .popup {
            display: none;
            position: fixed;
            left: 0;
            top: 0;
            height: 100vh;
            width: 100vw;
            background: rgba(255,255,255,0.9);
            cursor: pointer;
            justify-content: space-around;
            align-items: center;
          }

          .popup.show {
            display: flex;
          }

          .popup .inner {
            display: inline-block;
            position: relative;
          }

          .popup .inner img {
            max-height: 100vh;
            max-width: 100vw;
          }

          .popup .inner .close {
            background: white;
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            height: 30px;
            width: 30px;
            text-align: center;
            border-radius: 0 0 0 30%;
          }

          .popup .inner .close::after {
            content: "X";
          }
        `}
      </style>
      <div className="image" />
      <p>{alt}</p>
      <div className={`popup ${show && 'show'}`}>
        <div className="inner">
          <p className="close" />
          <img src={src} alt={alt} />
        </div>
      </div>
    </div>
  );
};

Image.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
};

export default Image;
