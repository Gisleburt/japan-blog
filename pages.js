const fs = require('fs');

const specialPages = [
  'index.jsx',
  '404.jsx',
];

const pages = fs.readdirSync('./pages') // Read all the files in pages
  .filter(page => !specialPages.includes(page)) // Remove index
  .map(name => name.replace('.jsx', '')); // Remove .jsx sufix

module.exports = pages;
