require('./gen-archive');
require('./gen-index');
const pages = require('./pages');

const specialPages = {
  '/': { page: '/' },
  '/404.html': { page: '/404' },
};
const paths = pages
  .map((name) => `/${name}`) // Add a slash to the beginning
  .reduce((acc, cur) => ({ ...acc, [cur]: { page: cur } }), specialPages);

module.exports = {
  exportPathMap: () => paths,
};
