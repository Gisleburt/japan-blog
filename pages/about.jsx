import React from 'react';
import Page from '../components/page';
import VrView from '../components/vrview';
import PostConfig from '../config/about';

const About = () => (
  <Page {...PostConfig}>
    <h2 className="description">{PostConfig.description}</h2>

    <VrView
      id="Sweeden360"
      image="/static/images/about/SAM_101_0105.jpg"
      default_yaw={230}
    />

    <p>
      We&apos;ve wanted to go to Japan for years but between time and money, the opportunity
      hasn&apos;t arisen, until now. We&apos;re spending the next three weeks in Osaka, Kyoto,
      Kanazawa and Tokyo, and we hope that we can share some of that time with you through this
      blog.
    </p>
  </Page>
);

export default About;
