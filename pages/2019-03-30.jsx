import React from 'react';
import Page from '../components/page';
import Image from '../components/image';
import VrView from '../components/vrview';
import PostConfig from '../config/2019-03-30';
import PostNavigation from '../components/post-navigation';

const Post = () => (
  <Page {...PostConfig}>
    <h2 className="description">{PostConfig.description}</h2>

    <p>
      We began our tour of the castle in the gardens where we were treated to beautiful pink and
      white Sakura and... the beginning of rain that lasted the rest of the day.
    </p>

    <Image
      alt="White sakura just inside the entrance to the castle grounds"
      src="/static/images/2019-03-30/white-sakura.jpg"
    />

    <p>
      Our spirits undampened, we had a tour of the Tomon-yagura Turret, which overlooks one of
      three entrances to the Castle grounds. We needed to remove our footwear and don provided
      slippers. Indra&apos;s were too big and Daniel&apos;s too small, walking without flicking
      them down the hall was challenging but mostly successful.
    </p>

    <Image
      alt="Tamon-Yagura as seen from the entrance ramp to the castle"
      src="/static/images/2019-03-30/tamon-yagura.jpg"
    />

    <p>
      We then had a wander through more of the gardens, and were treated to a beautiful look up
      to the main tower. We saw the gunpowder stores, a huge building with only a very small space
      inside. The walls of the building were 2.4 meters thick, and Daniel remarked at the time that
      they were planning for the worst. We later found out that the previous stores had been
      struck by lightning and set the whole castle ablaze.
    </p>

    <Image
      alt="The main, five story tower of Osaka Castle"
      src="/static/images/2019-03-30/osaka-castle-close.jpg"
    />

    <p>
      The main tower is five stories on the outside, although you enter from beneath the base so
      it&apos;s actually 8 stories inside. You can climb to the top or, if you&apos;re a whimp,
      take the elevator. We took the elevator. Inside you&apos;re treated to the story of the
      castle and it&apos;s creators. The top gives you beautiful views of Osaka.
    </p>

    <p>
      After the castle we headed to the nearby Osaka Museum. The museum covered everything from
      the lost palaces of Naniwa in the 7th century, to the effects of the second world war. This
      was one of the most magnificent museums we&apos;ve been to, so rather than prattle on,
      we&apos;ll just show you the pictures.
    </p>

    <Image
      src="/static/images/2019-03-30/osaka-museum-1.jpg"
      alt="Osaka Museum: 8th floor looking down to 7th"
    />
    <Image
      src="/static/images/2019-03-30/osaka-museum-2.jpg"
      alt="Osaka Museum: Street"
    />
    <Image
      src="/static/images/2019-03-30/osaka-museum-3.jpg"
      alt="Osaka Museum: Workshop"
    />
    <Image
      src="/static/images/2019-03-30/osaka-museum-4.jpg"
      alt="Osaka Museum: Woman working at home"
    />
    <Image
      src="/static/images/2019-03-30/osaka-museum-5.jpg"
      alt="Osaka Museum: Suburban home"
    />
    <Image
      src="/static/images/2019-03-30/osaka-museum-6.jpg"
      alt="Osaka Museum: Children shopping"
    />
    <Image
      src="/static/images/2019-03-30/osaka-museum-7.jpg"
      alt="Osaka Museum: Street"
    />
    <VrView
      id="OsakaMuseumVr"
      image="/static/images/2019-03-30/osaka-museum-vr.jpg"
      default_yaw={30}
    />

    <p>
      After heading home for a short rest we took our first trip to Dotonburi, an area of Osaka
      famous for its shopping. The best description we can give Londoners is: Camden, but with the
      sound, light and color turned to max.
    </p>

    <Image
      src="/static/images/2019-03-30/dotonburi-main.jpg"
      alt="Dotonburi Street"
    />

    <p>
      But we didn&apos;t stay long, we were looking for something specific. We headed down a little
      alleyway.
    </p>

    <Image
      src="/static/images/2019-03-30/dotonburi-side.jpg"
      alt="A (comparatively) dark alleyway with small bright shop signs"
    />

    <p>
      After a little hunting (google maps lost track of us) we eventually found it. Hozenji Sampei,
      allegedly the best Okonomiyaki in Osaka.
    </p>

    <Image
      src="/static/images/2019-03-30/okonomiyaki.jpg"
      alt="Okonomiyaki"
    />

    <p>
      Okonomiyaki is somewhere between a pancake and an omlette. Yaki means cooked and you&apos;ll
      see it with many famous Japanese delicacies. Okonomi means &quot;as you like&quot; because
      the dish can be made with a huge variety of ingredients. Daniel had cheese, bacon and mochi
      from the menu while Indra chose her own with egg, corn and potato. Both were delicious.
    </p>

    <PostNavigation previous="2019-03-29" next="2019-03-31" />

  </Page>
);

export default Post;
