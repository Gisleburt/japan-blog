import React from 'react';
import Page from '../components/page';
import Image from '../components/image';
import PostConfig from '../config/2019-04-02';
import PostNavigation from '../components/post-navigation';

const Post = () => (
  <Page {...PostConfig}>
    <h2 className="description">{PostConfig.description}</h2>

    <p>
      The city contains some amazing national treasures, some of which are UNESCO World Heritage
      Sites. In fact the city itself is also a UNESCO World Heritage Site. Our first stop, easing
      us into things, was Himuro Shrine.
    </p>

    <Image alt="Himuro Shrine" src="/static/images/2019-04-02/himuro-shrine.jpg" />

    <p>
      Shinto shrines are home to kami, the gods and spirits that are a part of nature. You can make
      an offering to the kami that lives there and ask for its blessing.
    </p>

    <p>
      Deer are considered sacred in Nara and roam the park freely. You may touch the deer and there
      are plenty of stalls that sell crackers for you to offer them. Be warned though, they are semi
      wild and may attack, though we didn&apos;t see anything like that while we were there. We did,
      however, see a few people getting chased for food!
    </p>

    <Image alt="Daniel with baby Deer" src="/static/images/2019-04-02/deer.jpg" />

    <p>
      The big attraction in Nara though is Toudai-ji, a huge temple complex in Nara Park. The
      center point of the complex is the Great Buddha Hall (Daibutsuden), an enormous building that
      contains a 16m Buddha statue (Daibutsu).
    </p>

    <Image alt="Daibutsuden from a distance" src="/static/images/2019-04-02/toudai-ji.jpg" />

    <p>
      On your way to the Great Hall you will likely pass through the Great South Gate. This gate
      has stood here since the twelfth century when it was rebuilt after a typhoon.
    </p>

    <Image alt="Great South Gate" src="/static/images/2019-04-02/great-south-gate.jpg" />

    <p>
      Outside of the Great Hall is Binzuru. If you have an ailment, and touch the affected body
      part on him, followed by the same body part on you, he&apos;s supposed to cure it. Indra
      couldn&apos;t reach his nose though so she still has hay-fever.
    </p>

    <Image alt="Binzuru the Healer" src="/static/images/2019-04-02/binzuru.jpg" />

    <p>
      Inside the Great Hall is a Daibutsu, giant Buddha statue, of Vairocana. At 16m tall, this is
      the largest statue of Vairocana in the world. On either side of Vairocana are Kokūzō Bosatsu
      and Nyoirin Kannon. These are not Buddhas, but holy people on the path to awakening. Flanking
      the trio are Komoku-ten and Bishamonten, guardians watching over the Toudai-ji.
    </p>

    <Image
      alt="The Great Buddha. Sooooo Biiiiggg! We don't think the picture does its size justice."
      src="/static/images/2019-04-02/the-great-buddha.jpg"
    />
    <Image alt="Kokuuzou Bosatsu " src="/static/images/2019-04-02/kokuuzou-bosatsu .jpg" />
    <Image alt="Komoku-ten Guardian of the West" src="/static/images/2019-04-02/komokuten.jpg" />
    <Image
      alt="Bishamonten Lord of Treasure and Wealth, Patron of Warriors"
      src="/static/images/2019-04-02/bishamonten.jpg"
    />

    <p>
      Next to the inner sanctum of the Daibutsuden used to stand two, 100m pagodas. These would have
      been some of the tallest structures in Japan at the time. One of the Sourin that would have
      topped the Pagoda survives and stands where the pagoda used to be.
    </p>

    <Image alt="Sourins usually top pagodas" src="/static/images/2019-04-02/sourin.jpg" />

    <p>
      On a hill overlooking the Great Hall is the bell tower.
    </p>

    <Image
      alt="The bell tower (you can see Daibutsuden in the background)"
      src="/static/images/2019-04-02/bell-tower.jpg"
    />
    <Image alt="Beautiful sakura near the bell tower" src="/static/images/2019-04-02/sakura.jpg" />

    <p>
      Within the Toudai-ji is a sub-complex called Nigatsu-dou. Every year they perform a service
      to the Bodhisattva Kannon, and they&apos;ve done it every year, without break, for over one
      thousand, three hundred years.
    </p>

    <Image alt="Nigatsu-dou" src="/static/images/2019-04-02/nigatsu-dou.jpg" />
    <Image alt="Nigatsu-dou rock garden" src="/static/images/2019-04-02/nigatsu-dou-rock-garden.jpg" />

    <p>
      After Toudai-ji we also went to another smaller Buddhist temple complex called Koufuku.
    </p>

    <Image
      alt="Koufuku-ji five story pagoda"
      src="/static/images/2019-04-02/koufuku-ji-pagoda.jpg"
    />
    <Image
      alt="Koufuku-ji side building"
      src="/static/images/2019-04-02/koufuku-ji-side-building.jpg"
    />

    <p>
      Finally, we headed to the famous mochi (Japanese rice cake) shop known as Nakatanidou. Their
      speciality is yomogi mochi. Yomogi, also known as mugwort,is a plant that grows wild in Japan
      and gives the mochi a wonderful green colour. Crowds flock to Nakatandiou to see the
      fascinating mochi-making process which involves highly synchronised (and rather dramatic)
      pounding. Unfortunately, we didn&apos;t get to witness the process, but we did get to sample
      the delightful mochi and can highly recommend it.
    </p>

    <p>
      <em>Interesting fact:</em> mochi can be deadly! By 4pm on the first of January 2019, 10
      Tokoyites had been taken to hospital after choking on their NYE mochi and unfortunately one
      died. Make sure to chew your mochi well before swallowing!
    </p>

    <Image
      alt="Potentially deadly, but highly delicious mochi"
      src="/static/images/2019-04-02/mochi.jpg"
    />

    <PostNavigation previous="2019-04-01" />
  </Page>
);

export default Post;
