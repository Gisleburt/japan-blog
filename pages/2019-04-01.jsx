import React from 'react';
import Page from '../components/page';
import Image from '../components/image';
import PostConfig from '../config/2019-04-01';
import PostNavigation from '../components/post-navigation';

const Post = () => (
  <Page {...PostConfig}>
    <h2 className="description">{PostConfig.description}</h2>

    <p>
      You may have noticed 館 mansion (or big house / small castle) also appears in Aquarium, above.
      The other characters are 水 water and 族 family. Daniel thinks this is cute. Indra does not.
    </p>

    <p>
      The aquarium was wonderful, though we did think some of the enclosures were on the small side
      for what they housed. Still, we enjoyed the sights, here are some of the highlights.
    </p>

    <Image alt="Clownfish" src="/static/images/2019-04-01/clownfish.jpg" />
    <Image alt="Diver" src="/static/images/2019-04-01/diver.jpg" />
    <Image alt="Giant Spider Crab" src="/static/images/2019-04-01/giant-spider-crab.jpg" />
    <Image alt="Little penguins" src="/static/images/2019-04-01/little-penguins.jpeg" />
    <Image alt="Mini eel things" src="/static/images/2019-04-01/mini-eel-things.jpg" />
    <Image
      alt="Otter bums. They were sleeping like this, we didn't see their little faces."
      src="/static/images/2019-04-01/otter-bums.jpeg"
    />
    <Image alt="Penguins" src="/static/images/2019-04-01/penguins.jpg" />
    <Image alt="Seal" src="/static/images/2019-04-01/seal.jpg" />
    <Image alt="Many Seals" src="/static/images/2019-04-01/seals.jpeg" />
    <Image
      alt="Whale Shark. At about four meters long, this was a little one"
      src="/static/images/2019-04-01/whale-shark.jpg"
    />
    <Image
      alt="WhatsApp Image 2019-04-01 at 22.07.49"
      src="/static/images/2019-04-01/WhatsApp Image 2019-04-01 at 22.07.49.jpeg"
    />
    <Image alt="20190401_131913" src="/static/images/2019-04-01/20190401_131913.jpg" />

    <p>
      For dinner we went to Daruma Kushi katsu, a &quot;skewer specialist&quot; which specialises
      in Kushikatsu. Kushi Katsu is a popular dish in Osaka of deep-fried skewered meat and
      vegetables. In Japanese, kushi refers to the skewers used while katsu means a deep-fried
      cutlet of meat.
    </p>

    <p>
      When we arrived at the restaurant there was a decently sized queue with a check-in terminal
      at the front of the waiting room. We tried to press the buttons on the touch screen but
      nothing was happening... So we observed some locals and garnered that you needed to use the
      pen attached to the screen. We checked-in and patiently waited for our number to be called.
      Our Japanese lessons had paid off and we felt very accomplished when we knew our number had
      been called.
    </p>

    <p>
      The restaurant was unlike anything we&apos;d experienced before. There is a screen on your
      table (thankfully with an English option) where you choose what you want to order. We went
      for quite a variety of different skewers ranging from strange vegetables such as lotus root,
      rice cakes and premium grade beef. We also treated ourselves to a small bottle of sparking
      mio sake each, which we thoroughly recommend. When your order is ready a red button lights
      up and a train whizzes by with your food on top, the train stops when it reaches your table.
      When you take the food off you push the red button to send the train speeding back to the
      wait staff.
    </p>

    <p>
      On the table was a small metal trough filled with delicious sauce for you to dip your crispy,
      golden skewers into. The number one rule of kushi katsu is absolutely NO double dipping.
      Once you have started to munch your skewer, that&apos;s it you can&apos;t add any more sauce.
    </p>

    <Image alt="Ordering" src="/static/images/2019-04-01/ordering.jpg" />
    <Image alt="Food in transit" src="/static/images/2019-04-01/food-in-transit.jpg" />

    <p>
      If you eat different things, we recommend you order separately. You can order as many times
      as you like, but there are no labels on the food when it comes.
    </p>

    <Image
      alt="Indra doesn't eat beef, but there's some in there somewhere."
      src="/static/images/2019-04-01/kushikatsu.jpg"
    />

    <p>
      Our favourite was the rice cake (the solitary white one above).
    </p>

    <PostNavigation previous="2019-03-31" next="2019-04-02" />
  </Page>
);

export default Post;
