import React from 'react';
import Page from '../components/page';
import Image from '../components/image';
import PostConfig from '../config/2019-03-31';
import PostNavigation from '../components/post-navigation';

const Post = () => (
  <Page {...PostConfig}>
    <h2 className="description">{PostConfig.description}</h2>

    <p>
      Konbinis are a place of joy and wonder, filled with food and drink to fulfil all your
      cravings. Indra had an Anpan, a Japanese Sweetroll filled with Red Bean paste, while Daniel
      had something akin to bao bread, in a swirl with cream cheese and ham.
    </p>

    <Image alt="Anpan" src="/static/images/2019-03-31/Anpan.jpeg" />
    <Image alt="Bao" src="/static/images/2019-03-31/Bao.jpg" />

    <p>
      After breakfast we headed back to Dotonbori, this time with the intention of actually
      shopping. Our first stop was Don Quijote, Japans largest discount store. This was quite the
      experience, six floors each rammed with goods ranging from food, to cosmetics, to electronics,
      to designer bags and for some reason, adult toys... on display... with &quot;Lets try!&quot;
      written on them.
    </p>

    <Image alt="Don Quijote" src="/static/images/2019-03-31/DonQuijote.jpg" />

    <p>
      We didn&apos;t. Instead we bought some gentle steaming eye masks for Indra&apos;s mum, a
      popular brand of suncream that&apos;s advertised everywhere and some acne cream, as well as a
      couple of Tarami Grape Konnyaku Jelly Drinks which come in a little pouch and is low in
      calories, and high in fibre. &quot;They&apos;re refreshing and tasty and filling and only 39
      calories&quot; says Indra who thinks we might have found &quot;the greatest diet snack of all
      time&quot;.
    </p>

    <Image alt="Lets try! Lets not!" src="/static/images/2019-03-31/LetsTry.jpg" />
    <Image alt="Discount Cosmetics" src="/static/images/2019-03-31/Cosmetics.jpeg" />
    <Image
      alt="&quot;The greatest diet snack of all time&quot;"
      src="/static/images/2019-03-31/Drink.jpg"
    />

    <p>
      Lunch was a quick bite to eat, a tofu salad and a pork raman. We also took an obligatory
      picture with Glico Man
    </p>

    <Image
      alt="Glico Man, a popular spot for a photo"
      src="/static/images/2019-03-31/GlicoMan.jpg"
    />

    <p>
      We went to the Sanrio store, famous for Hello Kitty, so that Indra could buy cute things,
      such as the pencil below. This was followed by a trip to Dongurikyowakoku, a shop that sells
      Studio Ghibli merchandise... so that Indra could but more cute things!
    </p>

    <Image alt="Cute Pencil" src="/static/images/2019-03-31/Pencil.jpg" />
    <Image alt="Indra in her idea of heaven" src="/static/images/2019-03-31/IndraTotoro.jpeg" />

    <p>
      Dongurikyowakoku was in Namba Walk, a 600 meter long underground shopping center. Here we
      took a quick break in Matcha House, a cafe specialising in Matcha based food and drink,
      followed by a taiyaki, a sweet pastry with red bean paste.
    </p>

    <Image alt="Matcha drinks" src="/static/images/2019-03-31/MatchaHouse.jpg" />
    <Image alt="Taiyaki is a fish shaped pastry" src="/static/images/2019-03-31/Taiyaki.jpg" />

    <p>
      In Den Den Town, an area known for tech shopping, Indra found some Gashapon with cute plushies
      inside. Gashapon are a kind of vending machine that are incredibly popular in Japan and serve
      everything from cute toys like the ones we bought, to adult only toys (we didn&apos;t find
      out what the &quot;female&apos;s secret weapon&quot; was).
    </p>

    <Image alt="Gashapon in its ball" src="/static/images/2019-03-31/Gashapon.jpeg" />
    <Image alt="Gashapon friends" src="/static/images/2019-03-31/GashaponFriends.jpeg" />

    <p>
      Finally, exhausted from walking about 13 kilometers on our shopping spree, we ate dinner at
      Imai Honton, highly recommended for their kitsune udon soup, which we both agree was the most
      おいしい (oishii, delicious) thing we&apos;ve enjoyed since we arrived.
    </p>

    <Image alt="A little alley next to Imai Honton" src="/static/images/2019-03-31/Alley.jpg" />
    <Image alt="Kitsune Udon" src="/static/images/2019-03-31/Udon.jpg" />
    <Image alt="Hidden Shrine near Imai Honton" src="/static/images/2019-03-31/HiddenShrine.jpg" />
    <Image alt="Lights near Imai Honton" src="/static/images/2019-03-31/Lights.jpg" />
    <Image alt="Shrine near Imai Honton" src="/static/images/2019-03-31/Shrine.jpg" />

    <p>
      Tomorrow we plan to have a more restful day.
    </p>

    <PostNavigation previous="2019-03-30" next="2019-04-01" />
  </Page>
);

export default Post;
