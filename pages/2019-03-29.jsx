import React from 'react';
import Page from '../components/page';
import Image from '../components/image';
import PostConfig from '../config/2019-03-29';
import PostNavigation from '../components/post-navigation';

const Post = () => (
  <Page {...PostConfig}>
    <h2 className="description">{PostConfig.description}</h2>

    <p>
      There was traffic everywhere and the taxi crawled along. We&apos;d reach a stretch with no traffic, and cheer
      inside thinking the journey would be plain sailing, only to then hit another sea of cars.
    </p>

    <p>
      Indra&apos;s a bit of a worrier, so kept frantically checking Google Maps to see long the journey time was. This
      seemed to keep increasing rather than going down! On the bright side, the driver took a rather scenic route
      through Richmond Park.
    </p>

    <Image
      src="/static/images/2019-03-23/goodbye-kitties.jpg"
      alt="Tinky and Yuki form a temporary truce to say goodbye"
    />

    <p>
      We finally arrived at the airport 20 minutes later than planned, but luckily still had enough time to check our
      bags, get through security and find breakfast (although we were still eating as we boarded the plane).
    </p>

    <p>
      From London we flew to Helsinki where we had a short stop over before getting our big flight to Osaka. Both
      flights were (compared to the taxi ride) mercifully uneventful. Three train rides later and we finally arrived
      at our hotel. Door to door, it was 21 hours.
    </p>

    <p>
      Tomorrow we aim to visit Osakajo (Osaka Castle), see the Sakura and the Kita area. We&apos;re off to bed now,
      and wish you all a, well... for those of you in the UK, good afternoon!
    </p>

    <p>More tomorrow!</p>

    <PostNavigation next="2019-03-30" />
  </Page>
);

export default Post;
