import React from 'react';
import Page from '../components/page';

const ErrorPage = () => (
  <Page path="/404">
    <style jsx>
      {`
        h2 {
          font-weight: bold;
        }
        .wrapper {
          display: grid;
          grid-template-columns: 1fr 1fr;
          height: 100%;
        }
        .centered {
          display: flex;
          justify-content: center;
          align-items: center;
        }
        .flag div {
          height: 100px;
          width: 100px;
          border-radius: 100%;
          background-color: red;
        }
      `}
    </style>
    <div className="wrapper">
      <div className="positioner centered">
        <h1>404</h1>
      </div>
      <div className="flag centered">
        <div />
      </div>
    </div>
  </Page>
);

export default ErrorPage;
