module.exports = {
  title: {
    native: '大阪城と博物館',
    english: 'Osaka Castle and Museum',
  },
  description: `
    Still recovering from the flight, we overslept by an hour or three this morning. Undeterred
    though, we had a quick breakfast at the コンビニ konbini (Japanese convenience store), and
    met the nicest lady at a drug store getting hay fever meds, before heading to the Castle...
  `,
  image: '/static/images/2019-03-30/osaka-castle-thin.jpg',
  path: '2019-03-30',
};
