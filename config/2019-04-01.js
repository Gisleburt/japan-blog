module.exports = {
  title: {
    native: '水族館',
    english: 'Aquarium',
  },
  description: `
    After the epic shopping fest of yesterday, we decided to take it easy today. We started with a
    nice lie in followed, as usual, with breakfast from the konbini. Then we headed off to the
    Kaiyukan Aquarium. Fun fact, Kaiyukan is formed from the Kanji characters 海 Sea 遊 Play and
    館 Mansion.
  `,
  image: '/static/images/2019-04-01/diver.jpg',
  path: '2019-04-01',
};
