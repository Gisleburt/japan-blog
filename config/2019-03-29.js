module.exports = {
  title: {
    native: '日本にいきます',
    english: 'Journey to Japan',
  },
  description: `
    Our journey got off to an... "exciting" start. The taxi was late, and didn't call to let us know. We
    waited for what seemed like an eternity (but was actually only 16 minutes). Daniel thought he'd factored in rush
    hour, but hadn't...
  `,
  image: '/static/images/2019-03-23/richmond-park.jpg',
  path: '2019-03-29',
};
