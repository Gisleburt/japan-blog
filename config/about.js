module.exports = {
  description: `
    Welcome to our Japan Holiday blog. We are Daniel and Indra, a software engineer and accountant from London,
    England, who were brought together by a love of all things geeky. We love to travel and where better for geeks to
    go than Japan?`,
  path: 'about',
};
