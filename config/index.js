module.exports = {
  domain: 'https://monogatari.life',
  title: {
    english: 'Our Story in Japan',
    native: '日本での話',
  },
  css: {
    breakpoint: {
      large: '(min-width: 1200px)',
      media: '(min-width: 800px)',
    },
    color: {
      title: '#333',
    },
    fontFamily: {
      header: {
        english: 'font-family: \'Lobster\', cursive;',
        native: 'font-family: \'Noto Serif SC\', serif;',
      },
    },
  },
  path: '',
};
