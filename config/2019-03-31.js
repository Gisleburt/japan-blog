module.exports = {
  title: {
    native: '買い物',
    english: 'Shopping',
  },
  description: `
    Today we woke up at 8:20 meaning we only snoozed the alarm for 20 minutes which, for us,
    is a great accomplishment. We headed to the kombini for breakfast which seems to be becoming
    our morning ritual. Everything you've heard about Japanese convenience stores is true...
  `,
  image: '/static/images/2019-03-31/DonQuijote.jpg',
  path: '2019-03-31',
};
