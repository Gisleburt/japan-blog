module.exports = {
  title: {
    native: '奈良',
    english: 'Nara',
  },
  description: `
    Today we headed out to Nara, Japan's first permanent Capital. Although some temples already
    existed in what's now Nara, the town was created specifically for the purpose of serving the
    capital when it moved here at the start of the 8th century...
  `,
  image: '/static/images/2019-04-02/toudai-ji.jpg',
  path: '2019-04-02',
};
